package main;

public class People {
    private int age;
    private String gender;
    private String status;
    private String name;

    private People() {
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }
    public static Builder newBuilder() {
        return new People().new Builder();
    }

    public class Builder{
        private Builder() {
        }
        public Builder setAge(int age) {
            if(age<0) {
                System.out.println("Невозможный возраст : " + age);
            }
            if(age>=0){
                People.this.age = age;
                return this;
            }
            return this;
        }

        public Builder setGender(String gender) {
            if (gender.equals("Man") || gender.equals("Male")
                    || gender.equals("Female") || gender.equals("Woman")) {
                People.this.gender = gender;
                return this;
            } else {
                String other = "Неверно указан пол";
                People.this.gender = gender;
                return this;
            }
        }

        public Builder setStatus(String status) {
            People.this.status = status;
            return this;
        }
        public Builder setName(String name) {
            People.this.name = name;
            return this;
        }
        public People built(){
            return People.this;
        }


    }
}
