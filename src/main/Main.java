package main;

public class Main {
    public static void main(String[] args) {
        People people = People.newBuilder()
                .setGender("Male")
                .setName("Alex")
                .setAge(23)
                .setStatus("Consumer")
                .built();
        System.out.println(
                "Gender: "+people.getGender()+"\n"+
                "Name: "+people.getName()+"\n"+
                "Age: "+people.getAge()+"\n"+
                "Status: "+people.getStatus()
        );
    }

}
